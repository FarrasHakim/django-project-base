var nonactiveimg = "../images/place-nonactive.png";

var nodes = [];
var edges = [];
var network = null;

// Called when the Visualization API is loaded.
function draw() {
    // create people.
    // value corresponds with the age of the person
    var DIR = "https://i.ibb.co/vYSVkRb/place-nonactive.png";

    $.ajax({
        url: '/nodes/datas/',
        type: 'GET',
        data: {
            node_start: $('#inputAsal').val(),
            node_end: $('#inputTujuan').val(),
        },
        success: function(response) {
            // create node
            for (var i = 0; i < response.nodes.length; i++) {
                nodes[i] = { id: i + 1, shape: "circularImage", image: DIR, label: response.nodes[i] };
            }

            // create connections between nodes
            // value corresponds with node's id that edges between two nodes are defined
            for (var j = 0; j < response.distances.length; j++) {
                var node;
                var toId;
                var toId;
                for (node of nodes) {
                    if (node.label == response.distances[j].nodeFrom) {
                        fromId = node.id;
                    }
                    if (node.label == response.distances[j].nodeTo) {
                        toId = node.id;
                    }
                }
                edges[j] = { from: fromId, to: toId };
            }

            // create network, nodes with edges
            var container = document.getElementById("mynetwork");
            var data = {
                nodes: nodes,
                edges: edges
            };
            var options = {
                nodes: {
                    borderWidth: 1,
                    size: 30,
                    color: {
                        background: "#301551"
                    },
                    font: { color: "#301551" }
                },
                edges: {
                    color: "#301551"
                }
            };
            network = new vis.Network(container, data, options);
            console.log(nodes);
        }
    });

    // nodes = [];
    // for (var i = 1;  i <=  nodes.length; i++) {
    //   nodes[i] = { id: i, shape: "circularImage", image: DIR, label: nodes.name};
    // }

    // nodes = [
    //     { id: 1, shape: "circularImage", image: DIR, label: "Fakultas A" },
    //     { id: 2, shape: "circularImage", image: DIR, label: "Fakultas B" },
    //     { id: 3, shape: "circularImage", image: DIR, label: "Fakultas C" },
    //     { id: 4, shape: "circularImage", image: DIR, label: "Fakultas D" },
    //     { id: 5, shape: "circularImage", image: DIR, label: "Fakultas E" },
    //     { id: 6, shape: "circularImage", image: DIR, label: "Fakultas F" },
    //     { id: 7, shape: "circularImage", image: DIR, label: "Fakultas G" },
    //     { id: 8, shape: "circularImage", image: DIR, label: "Fakultas H" },
    //     { id: 9, shape: "circularImage", image: DIR, label: "Fakultas I" },
    //     { id: 10, shape: "circularImage", image: DIR, label: "Fakultas J" },
    // ];

    // edges = [
    //     { from: 1, to: 2 },
    //     { from: 2, to: 3 },
    //     { from: 7, to: 4 },
    //     { from: 4, to: 5 },
    //     { from: 2, to: 5 },
    //     { from: 7, to: 6 },
    //     { from: 9, to: 3 },
    //     { from: 7, to: 8 },
    //     { from: 4, to: 8 },
    //     { from: 6, to: 10 },
    //     { from: 8, to: 9 },
    // ];
}

window.addEventListener('load', function() {
    console.log('calling draw');
    draw();
});